﻿using UnityEngine;
using System.Collections;

public class MusicManager: MonoBehaviour {

	public AudioClip[] levelMusic;
	private AudioSource musicPlayer;

	void Awake(){
		GameObject.DontDestroyOnLoad(gameObject);
	}

	void Start(){
		musicPlayer = GetComponent<AudioSource>();
		musicPlayer.volume = PlayerPrefsManager.GetMasterVolume();
	}

	void OnLevelWasLoaded(int level){
		if(!musicPlayer) return;
		musicPlayer.Stop();
		musicPlayer.clip = levelMusic[level-1];
		musicPlayer.loop = true;
		musicPlayer.Play();
	}

	public void SetVolume(float volume){
		musicPlayer.volume = volume;
	}
}
