﻿using UnityEngine;
using System.Collections;

public class DefenderSpawner : MonoBehaviour {

	private GameObject mainCam;
	private GameObject defendersParent;
	private StarDisplay sd;

	void Start () {
		sd = GameObject.FindObjectOfType<StarDisplay>();
		defendersParent = GameObject.Find ("Defenders");
		if(defendersParent == null){
			defendersParent = new GameObject("Defenders");
		}
		mainCam = GameObject.Find ("Game Camera");
	}

	void OnMouseDown(){
		if(Button.currentSelected!=null){
			if(StarDisplay.Status.SUCESS == sd.UseStars(Button.currentSelected.GetComponent<Defender>().starCost)){
				GameObject newDefender = Instantiate(Button.currentSelected, SnapToGrid(CalculateWorldPointOfMouseClick()), Quaternion.identity) as GameObject;
				newDefender.transform.SetParent(defendersParent.transform);
			}else{
				Debug.Log("Not enough resources.");
			}
		}
	}

	Vector2 CalculateWorldPointOfMouseClick(){
		Vector2 point = mainCam.GetComponent<Camera>().ScreenToWorldPoint(Input.mousePosition);
		return point;
	}

	Vector2 SnapToGrid(Vector2 rawWorldPos){
		return new Vector2(Mathf.Round(rawWorldPos.x),Mathf.Round(rawWorldPos.y));
	}
}
