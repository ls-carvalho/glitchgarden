﻿using UnityEngine;
using System.Collections;

public class Shooter : MonoBehaviour {

	public GameObject projectile;

	private GameObject projectileParent;
	private Animator anim;
	private AttackerSpawner myLaneSpawner;

	void Start () {
		anim = gameObject.GetComponent<Animator>();
		projectileParent = GameObject.Find("Projectiles");
		if(projectileParent == null){
			projectileParent = new GameObject("Projectiles");
		}
		SetMyLane();
	}

	void Update () {
		if(AttackerSpotted()){
			anim.SetBool("isAttacking", true);
		}else{
			anim.SetBool("isAttacking", false);
		}
	}

	void SetMyLane(){
		AttackerSpawner[] spawnerArray = GameObject.FindObjectsOfType<AttackerSpawner>();
		foreach ( AttackerSpawner thisSpawner in spawnerArray){
			if(transform.position.y == thisSpawner.transform.position.y){
				myLaneSpawner = thisSpawner;
			}
		}
		if(myLaneSpawner==null){
			Debug.LogError("Could not find a spawner in lane.");
		}
	}

	bool AttackerSpotted(){
		if(myLaneSpawner==null||myLaneSpawner.gameObject.transform.childCount<=0){
			return false;
		}else{
			foreach(Transform child in myLaneSpawner.transform){
				if(child.transform.position.x >= this.transform.position.x){
					return true;
				}
			}
			return false;
		}
	}

	private void FireGun(){
		GameObject newProjectile = Instantiate (projectile, gameObject.transform.GetChild(1).transform.position, Quaternion.identity) as GameObject;
		newProjectile.transform.SetParent(projectileParent.transform);
	}
}
