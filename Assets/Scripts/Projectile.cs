﻿using UnityEngine;
using System.Collections;

public class Projectile : MonoBehaviour {

	public float speed, damage;

	void Update () {
		transform.Translate (Vector3.right * speed * Time.deltaTime);
	}

	void DoingDamage(GameObject enemy){
		enemy.GetComponent<Health>().myHealth-=damage;
	}
	
	void OnTriggerEnter2D(Collider2D collider){
		GameObject aim = collider.gameObject;
		if(!aim.GetComponent<Attacker>()){
			return;
		}
		DoingDamage(aim);
		Destroy(gameObject);
	}
}
