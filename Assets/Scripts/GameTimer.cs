﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class GameTimer : MonoBehaviour {

	[Tooltip ("Time for the level to be completed, in seconds.")]
	public float levelTime = 60f;

	private Slider timer;
	private LevelManager lm;
	private AudioSource audioPlayer;
	private bool isEndOfLevel = false;
	private GameObject winLabel;

	void Start () {
		winLabel = GameObject.Find ("WinText");
		if(!winLabel){
			Debug.Log ("Missing an WinText label.");
		}else{
			winLabel.SetActive(false);
		}
		audioPlayer = gameObject.GetComponent<AudioSource>();
		lm = GameObject.FindObjectOfType<LevelManager>();
		timer = GetComponent<Slider>();
	}

	void Update () {
		timer.value =  Time.timeSinceLevelLoad/levelTime;
		if(timer.value == 1 && !isEndOfLevel){
			Survived();
			isEndOfLevel = true;
		}
	}

	void Survived(){
		Invoke("StageWon",audioPlayer.clip.length);
		winLabel.SetActive(true);
		audioPlayer.Play();
		DestroyTaggedObjects();
		TurnOffSpawn();
	}

	void DestroyTaggedObjects(){
		GameObject[] destroyArray = GameObject.FindGameObjectsWithTag("destroyOnWin");
		foreach (GameObject objectToDestroy in destroyArray){
			GameObject.Destroy(objectToDestroy);
		}
	}

	void TurnOffSpawn(){
		AttackerSpawner[] spw = GameObject.FindObjectsOfType<AttackerSpawner>();
		foreach (AttackerSpawner spawner in spw){
			spawner.TurnOff();
		}
	}

	void StageWon(){
		lm.LoadNextLevel();
	}
}
