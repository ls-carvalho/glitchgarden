﻿using UnityEngine;
using System.Collections;

public class LoseCollider : MonoBehaviour {

	private LevelManager lm;

	void Start () {
		lm = GameObject.FindObjectOfType<LevelManager>();
	}

	void OnTriggerEnter2D(){
		lm.LoadLevel("03b LoseScreen");
	}
}
