﻿using UnityEngine;
using System.Collections;

public class PlayerPrefsManager : MonoBehaviour {

	const string MASTER_VOLUME_KEY = "master_volume";
	const string DIFFICULTY_KEY = "difficulty";
	const string LEVEL_KEY = "level_unlocked_";

	public static void SetMasterVolume(float volume){
		if((0f<=volume)&&(volume<=1f)){
			PlayerPrefs.SetFloat(MASTER_VOLUME_KEY,volume);
		}else{
			Debug.LogError("Volume is out of range.");
		}
	}

	public static float GetMasterVolume(){
		return PlayerPrefs.GetFloat(MASTER_VOLUME_KEY);
	}

	public static void UnlockLevel(int level){
		if(level <= Application.levelCount -1){
			PlayerPrefs.SetInt(LEVEL_KEY+level.ToString(), 1); // 1 for true
		}else{
			Debug.LogError("Level is beyond reach.");
		}
	}

	public static bool IsLevelUnlocked(int level){
		if(level<=Application.levelCount-1){
			if(1==PlayerPrefs.GetInt(LEVEL_KEY+level.ToString())){
				return true;
			}else{
				return false;
			}
		}else{
			Debug.LogError("Level is beyond reach.");
			return false;
		}
	}

	public static void SetDifficulty(float difficulty){
		if((1f<=difficulty)&&(difficulty<=3f)){
			PlayerPrefs.SetFloat(DIFFICULTY_KEY,difficulty);
		}else{
			Debug.LogError("Difficulty is out of range.");
		}
	}
	
	public static float GetDifficulty(){
		return PlayerPrefs.GetFloat(DIFFICULTY_KEY);
	}
}
