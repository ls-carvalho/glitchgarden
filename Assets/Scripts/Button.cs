﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Button : MonoBehaviour {

	public static GameObject currentSelected;

	public GameObject defenderPrefab;

	private SpriteRenderer sr;
	private Text cost;

	void Start(){
		currentSelected = null;
		sr = gameObject.GetComponent<SpriteRenderer>();
		cost = GetComponentInChildren<Text>();
		if(cost!=null){
			cost.text = defenderPrefab.GetComponent<Defender>().starCost.ToString();
		}else{
			Debug.LogError("Could not find the cost for "+gameObject+" button.");
		}
	}

	void Update(){
		if(currentSelected!=defenderPrefab){
			sr.color = Color.black;
		}
	}

	void OnMouseDown(){
		if(sr.color == Color.white){
			currentSelected = null;
			sr.color = Color.black;
		}else{
			currentSelected = defenderPrefab;
			sr.color = Color.white;
		}
	}
}
