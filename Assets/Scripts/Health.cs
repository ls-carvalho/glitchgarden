﻿using UnityEngine;
using System.Collections;

public class Health : MonoBehaviour {

	public float myHealth;

	void Update(){
		if(myHealth <= 0){
			ObjectDying();
		}
	}

	public void ObjectDying(){
		Destroy(gameObject);
	}
}
