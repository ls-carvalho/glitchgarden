﻿using UnityEngine;
using System.Collections;

public class AttackerSpawner : MonoBehaviour {

	public GameObject[] attackersArray;
	public float timeToStart = 0;
	public bool allowedToSpawn = true;

	void Update () {
		foreach(GameObject thisAttacker in attackersArray){
			if(IsTimeToSpawn(thisAttacker)){
				Spawn(thisAttacker);
			}
		}
	}

	void Spawn(GameObject myGameObject){
		GameObject newOne = Instantiate(myGameObject, this.transform.position, Quaternion.identity) as GameObject;
		newOne.transform.SetParent(this.transform);
	}

	bool IsTimeToSpawn(GameObject myGameObject){
		if(Time.timeSinceLevelLoad>timeToStart&&allowedToSpawn){
			Attacker mgo = myGameObject.GetComponent<Attacker>();
			if(Random.value < Time.deltaTime*(Time.timeSinceLevelLoad/GameObject.FindObjectOfType<GameTimer>().levelTime)/5*mgo.rarity){
				return true;
			}else{
				return false;
			}
		}else{
			return false;
		}
	}

	public void TurnOff(){
		allowedToSpawn = false;
	}
}
