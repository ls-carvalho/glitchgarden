﻿using UnityEngine;
using System.Collections;

public class LevelManager : MonoBehaviour {

	public float timeToLoad;
	private FadeControl fc;

	void Start(){
		if(Application.loadedLevel<=0){
			fc = GameObject.FindObjectOfType<FadeControl>();
			Invoke("LoadNextLevel",timeToLoad);
			fc.Fade(true);
			Invoke("InvFade",timeToLoad-fc.timeToFadeOut);
		}
	}

	public void LoadLevel(string name){
		Application.LoadLevel(name);
	}
	
	public void LoadNextLevel(){
		Application.LoadLevel(Application.loadedLevel+1);
	}

	void InvFade(){
		fc.Fade(false);
	}
}
