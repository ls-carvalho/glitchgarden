﻿using UnityEngine;
using System.Collections;

public class Defender : MonoBehaviour {

	public int starCost = 100;

	private StarDisplay sd;

	void Start(){
		sd = GameObject.FindObjectOfType<StarDisplay>();
	}

	public void AddStars(int amount){
		sd.AddStars(amount);
	}
}
