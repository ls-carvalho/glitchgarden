﻿using UnityEngine;
using System.Collections;

[RequireComponent (typeof(Attacker))]
public class Fox : MonoBehaviour {

	private Animator anim;
	private Attacker atta;

	void Start () {
		anim = GetComponent<Animator>();
		atta = GetComponent<Attacker>();
	}

	void OnTriggerEnter2D(Collider2D collider){
		GameObject obj = collider.gameObject;
		if(!obj.GetComponent<Defender>()){
			return;
		}
		if(obj.GetComponent<Gravestone>()){
			anim.SetTrigger("jumpTrigger");
		}else{
			anim.SetBool("isAttacking", true);
			atta.Attack(obj);
		}
	}
}
