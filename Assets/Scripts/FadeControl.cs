﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class FadeControl : MonoBehaviour {

	public static bool firstTime = true;
	public bool fadeOnStart = false;
	public float timeToFadeIn = 1f;
	public float timeToFadeOut = 1f;
	public Image panelImage;
	
	void Start(){
		if(fadeOnStart&&firstTime){
			Fade(true);
			Invoke("DisablePanel",timeToFadeIn);
			firstTime=false;
		}else if(!firstTime){
			DisablePanel();
		}
	}

	void DisablePanel(){
		gameObject.SetActive(false);
	}

	public void Fade(bool state)
	{
		StartCoroutine(FadeImage(state));
	}
	
	IEnumerator FadeImage(bool fadeAway)
	{
		if (fadeAway)
		{
			for (float i = 1; i >= 0; i -= Time.deltaTime/timeToFadeIn)
			{
				panelImage.color = new Color(0, 0, 0, i);
				yield return null;
			}
		}
		else
		{
			for (float i = 0; i <= 1; i += Time.deltaTime/timeToFadeOut)
			{
				panelImage.color = new Color(0, 0, 0, i);
				yield return null;
			}
		}
	}
}