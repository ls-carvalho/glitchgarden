﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
[RequireComponent (typeof(Text))]
public class StarDisplay : MonoBehaviour {

	public int initialStars = 100;
	public enum Status {SUCESS, FAILURE};

	private Text starText;
	private int starAux;

	void Start () {
		starText = gameObject.GetComponent<Text>();
		starAux = initialStars;
		UpdateDisplay();
	}

	public void AddStars (int amount){
		starAux += amount;
		UpdateDisplay();
	}

	public Status UseStars (int amount){
		if(starAux>=amount){
			starAux -= amount;
			UpdateDisplay();
			return Status.SUCESS;
		}else{
			return Status.FAILURE;
		}
	}

	private void UpdateDisplay(){
		starText.text = starAux.ToString();
	}
}
