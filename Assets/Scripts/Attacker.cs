﻿using UnityEngine;
using System.Collections;

[RequireComponent (typeof(Rigidbody2D))]
public class Attacker : MonoBehaviour {

	[Tooltip ("The frequency witch it will be seen in gamescreen.")]
	public float rarity;

	private float currentSpeed;
	private GameObject currentTarget;
	private Health enemyHealth;
	private Animator anim;

	void Start(){
		anim = gameObject.GetComponent<Animator>();
	}

	void Update () {
		transform.Translate(Vector3.left * currentSpeed * Time.deltaTime);
		if(!currentTarget&&anim.GetBool("isAttacking")){
			anim.SetBool("isAttacking", false);
		}
	}

	public void SetSpeed(float speed){
		currentSpeed = speed;
	}

	public void StrikeCurrentTarget(float damage){
		enemyHealth.myHealth-=damage;
	}

	public void Attack(GameObject obj){
		currentTarget = obj;
		enemyHealth = currentTarget.GetComponent<Health>();
	}
}
